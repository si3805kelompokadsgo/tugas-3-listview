package com.example.android.listviewadsgo;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    ListView list;
    String[] namapaket;
    String[] hargapaket;
    int[] imgs = {R.drawable.driver_produk1, R.drawable.driver_produk2, R.drawable.driver_produk3, R.drawable.driver_produk4,
            R.drawable.driver_produk_full2, R.drawable.driver_produk_full5, R.drawable.driver_produk_full7};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Resources res = getResources();
        namapaket = res.getStringArray(R.array.namaPaket);
        hargapaket = res.getStringArray(R.array.hargaPaket);
        list = (ListView) findViewById(R.id.list1);
        MyAdapter adapter = new MyAdapter(this, namapaket, imgs, hargapaket);
        list.setAdapter(adapter);


    }

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        int[] images;
        String[] nama_paket;
        String[] harga_paket;

        MyAdapter(Context c, String[]nama, int imgs[], String[]harga)
        {
            super(c, R.layout.row_paket, R.id.teks1, nama);
            this.context=c;
            this.images = imgs;
            this.nama_paket = nama;
            this.harga_paket = harga;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.row_paket, parent, false);

            ImageView myImage = (ImageView) row.findViewById(R.id.gambar);
            TextView myNamaPaket = (TextView) row.findViewById(R.id.teks1);
            TextView myHargaPaket = (TextView) row.findViewById(R.id.teks2);

            myImage.setImageResource(images[position]);
            myNamaPaket.setText(nama_paket[position]);
            myHargaPaket.setText(harga_paket[position]);
            return row;
        }
    }

}