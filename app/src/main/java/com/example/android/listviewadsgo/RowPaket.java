package com.example.android.listviewadsgo;

/**
 * Created by acer on 5/18/2017.
 */

public class RowPaket {

    private String namapaket;
    private String hargapaket;

    public RowPaket(String namapaket, String hargapaket) {
        this.namapaket = namapaket;
        this.hargapaket = hargapaket;
    }

    public String getNamapaket() {
        return namapaket;
    }

    public void setNamapaket(String namapaket) {
        this.namapaket = namapaket;
    }

    public String getHargapaket() {
        return hargapaket;
    }

    public void setHargapaket(String hargapaket) {
        this.hargapaket = hargapaket;
    }
}
